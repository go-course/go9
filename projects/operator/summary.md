# 



1. 项目初始化
```
#mkdir -p ~/projects/guestbook
#cd ~/projects/guestbook
#kubebuilder init --domain my.domain --repo my.domain/guestbook
kubebuilder init --repo operator --domain go9
```

2. 定义API

生成代码
```
# mpaas/v1 Pipeline
# https://api.github.com/repos/kubernetes-sigs/kustomize/releases
# 如果zz_generated.deepcopy.go没生成 就得自己生成
# controller-gen object:headerFile="hack\\boilerplate.go.txt" paths="./..."
kubebuilder create api --group mpaas --version v1 --kind Pipeline
```

API 的数据结构，定义一个对象(Runtime Object: struct): Edit the API Scheme
```
# pipeline_types.go

```

重新生成CRD描述文件，和新结构体对应的代码
```
  manifests        Generate WebhookConfiguration, ClusterRole and CustomResourceDefinition objects.
  generate         Generate code containing DeepCopy, DeepCopyInto, and DeepCopyObject method implementations.
```

