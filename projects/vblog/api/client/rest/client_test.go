package rest_test

import (
	"context"
	"fmt"
	"testing"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/blog"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/client/rest"
	"github.com/infraboard/mcube/logger/zap"
)

var (
	client *rest.Client
	ctx    = context.Background()
)

// 类似于通过Postman 来调用我们的服务端
func TestCreateBlog(t *testing.T) {
	req := blog.NewCreateBlogRequest()
	req.Author = "oldfish"
	req.Title = "SDK Test2"
	req.Summary = "Go Javascript"
	req.Content = "baba"
	ins, err := client.CreateBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryBlog(t *testing.T) {
	req := blog.NewQueryBlogRequest()
	ins, err := client.QueryBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func init() {
	// restful client 使用zap库, 要打印debug信息, 需要设置zap
	zap.DevelopmentSetup()
	conf := rest.NewDefaultConfig()
	client = rest.NewClient(conf)

	if err := client.Login(); err != nil {
		panic(err)
	}

	fmt.Println(client.Session())
}
