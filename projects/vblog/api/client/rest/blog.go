package rest

import (
	"context"
	"fmt"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/blog"
)

// 这里的这个函数和服务端的函数是什么关系
// JSON ON HTTP RPC   http client --->request ----> http server(restful API)
func (c *Client) CreateBlog(ctx context.Context, in *blog.CreateBlogRequest) (
	*blog.Blog, error) {
	// resp, _ := http.Post("/vblog/api/v1", nil)
	// body, _ := io.ReadAll(resp.Body)
	// body ---> blog.Blog

	// Object --> JSON
	// {
	// 	"title": "web test",
	// 	"author": "oldfish",
	// 	"summary": "http api",
	// 	"content": "web service"
	// }

	// Into: Response ---> Objcet
	// {
	// 	"data": {
	// 		"id": 9,
	// 		"created_at": 1669455109,
	// 		"updated_at": 1669455109,
	// 		"pulished_at": 0,
	// 		"title": "web tes111t",
	// 		"author": "oldfish",
	// 		"summary": "http api",
	// 		"content": "web service",
	// 		"status": "draft"
	// 	}
	// }

	//
	// restclient lib, 实现了链式调用的HTTP Restful client 风格是k8s restful API 的定制
	//
	ins := &blog.Blog{}
	err := c.c.Post("blogs").
		Body(in).
		Do(ctx).
		Into(ins)
	if err != nil {
		return nil, err
	}

	return ins, nil
}

// 这里的这个函数和服务端的函数是什么关系
// JSON ON HTTP RPC   http client --->request ----> http server(restful API)
func (c *Client) QueryBlog(ctx context.Context, in *blog.QueryBlogRequest) (
	*blog.BlogSet, error) {
	//
	// restclient lib, 实现了链式调用的HTTP Restful client 风格是k8s restful API 的定制
	//
	// k8s Restful Client  C().Namespace().Name().Action().DO().Into().
	// http.Post()
	set := &blog.BlogSet{}
	err := c.c.Get("blogs").
		Param("page_size", fmt.Sprintf("%d", in.PageSize)).
		Param("page_number", fmt.Sprintf("%d", in.PageNumber)).
		Do(ctx).
		Into(set)
	if err != nil {
		return nil, err
	}

	return set, nil
}
