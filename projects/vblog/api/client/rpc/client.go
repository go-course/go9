package rpc

import (
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/comment"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func NewClientSet(address string) (*ClientSet, error) {
	// grpc.Dial负责和gRPC服务建立链接
	// use WithTransportCredentials and insecure.NewCredentials() instead. Will be supported throughout 1.x.
	conn, err := grpc.Dial(address, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	return &ClientSet{
		conn: conn,
	}, nil
}

// 访问这个服务的说有 grpc接口
type ClientSet struct {
	// grpc连接
	conn *grpc.ClientConn
}

func (c *ClientSet) Comment() comment.RPCClient {
	return comment.NewRPCClient(c.conn)
}
