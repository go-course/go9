package initial

import (
	"github.com/spf13/cobra"
)

// Init cmd
var Cmd = &cobra.Command{
	// 程序名称
	Use:     "init",
	Short:   "init vblog-api server",
	Long:    "启动服务",
	Example: "vblog-api init -f etc/config.toml",
	// 命令执行逻辑
	// The *Run functions are executed in the following order:
	//   * PersistentPreRun()
	//   * PreRun()
	//   * Run()
	//   * PostRun()
	//   * PersistentPostRun()
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}
