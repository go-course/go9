package main

import (
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/cmd"
	"github.com/spf13/cobra"

	// 加载所有的实例类
	_ "gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/all"
)

// 程序的入口: 指向CLI ROOT
func main() {
	// 执行Root命令
	err := cmd.RootCmd.Execute()
	cobra.CheckErr(err)
}
