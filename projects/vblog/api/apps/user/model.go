package user

func NewSession(username, sess string) *Session {
	return &Session{
		UserName: username,
		Id:       sess,
	}
}

type Session struct {
	UserName string `json:"username"`
	Id       string `json:"session"`
}
