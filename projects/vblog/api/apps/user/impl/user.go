package impl

import (
	"context"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/user"
)

// 用户登录
func (i *impl) Login(ctx context.Context, req *user.LoginRequest) (
	*user.Session, error) {
	// 比对用户名和密码, 和谁比对
	if err := i.Auth.Validate(req.Name, req.Pass); err != nil {
		return nil, err
	}

	// 记录一个session
	sess := i.createSession(req.Name)

	return user.NewSession(req.Name, sess), nil
}

// 用户登出
func (i *impl) Logout(ctx context.Context, req *user.LogoutRequest) error {
	i.deleteSession(req.Username)
	return nil
}
