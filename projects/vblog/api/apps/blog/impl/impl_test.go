package impl_test

import (
	"context"
	"testing"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/blog"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/test/tools"
)

// 单元测试需要测试的对象是什么?
var (
	// 测试实现了Blog Service的对象
	impl blog.Service
	ctx  = context.Background()
)

func TestCreateBlog(t *testing.T) {
	req := blog.NewCreateBlogRequest()
	req.Author = "oldfish"
	req.Title = "Go Course3"
	req.Summary = "Go Javascript"
	req.Content = "baba"
	ins, err := impl.CreateBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(ins)
}

func TestQueryBlog(t *testing.T) {
	req := blog.NewQueryBlogRequest()
	req.Keywords = "Go"
	// req.PageSize = 1
	// req.PageNumber = 2
	// SELECT * FROM `blogs` WHERE title LIKE '%Go%' ORDER BY created_at DESC LIMIT 20
	// SELECT count(*) FROM `blogs` WHERE title LIKE '%Go%' LIMIT 1 OFFSET 1
	ins, err := impl.QueryBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(ins)
}

func TestDescribeBlog(t *testing.T) {
	req := blog.NewDescribeBlogRequest(1)
	// SELECT * FROM `blogs` WHERE id = 3
	ins, err := impl.DescribeBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(ins)
}

func TestPutUpdateBlog(t *testing.T) {
	data := blog.NewCreateBlogRequest()
	data.Author = "oldfish"
	data.Content = "Put Test"
	data.Summary = "Put Test"
	data.Title = "Put Test"
	req := blog.NewPutUpdate(3, data)
	ins, err := impl.UpdateBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(ins)
}

func TestPatchUpdateBlog(t *testing.T) {
	data := blog.NewCreateBlogRequest()
	data.Title = "Type Script"
	req := blog.NewPatchUpdate(3, data)
	// UPDATE `blogs` SET `created_at`=1669445556,`updated_at`=1669450170,`title`='Type Script',`author`='oldfish',`summary`='Go Javascript',`content`='baba' WHERE id = 3 AND `id` = 3
	ins, err := impl.UpdateBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(ins)
}

func TestDeleteBlog(t *testing.T) {
	req := blog.NewDeleteBlogRequest(5)
	// DELETE FROM `blogs` WHERE `blogs`.`id` = 5
	ins, err := impl.DeleteBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(ins)
}

// 引用这个包 就会执行, 用于包的初始化
// 测试用例的包，需要初始化什么?
// 需要初始化被测试的对象
func init() {
	// 这个对象在ioc, ioc 需要提前初始化
	// 加载测试用例的配置对象
	tools.DevelopmentSet()
	// ioc里面获取的blog service的具体实现
	impl = apps.GetInternalApp(blog.AppName).(blog.Service)
}
