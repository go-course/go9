package impl

import (
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/blog"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/tag"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/conf"
	"gorm.io/gorm"
)

// 文章管理实例类
type impl struct {
	tag tag.Service

	// 也是一个连接池，是gorm封装过后的连接池
	db *gorm.DB
}

// 对实例进行初始化，保护依赖的注入
func (i *impl) Init() error {
	// 通过ioc完成依赖的手动注入
	i.tag = apps.GetInternalApp(tag.AppName).(tag.Service)

	// 需要赋值又给db实例过来, db实例是全局共享的
	// debug 会把执行的sql 打印处理
	i.db = conf.C().MySQL.ORM().Debug()
	return nil
}

// 其他地方还需要依赖这个名字
func (i *impl) Name() string {
	return blog.AppName
}

func init() {
	// 指标把对象托管到Ioc
	apps.Registry(&impl{})
}
