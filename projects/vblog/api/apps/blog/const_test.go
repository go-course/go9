package blog_test

import (
	"encoding/json"
	"testing"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/blog"
)

func TestStatusMarshler(t *testing.T) {
	b := &blog.Blog{
		CreateBlogRequest: blog.NewCreateBlogRequest(),
	}
	dj, err := json.Marshal(b)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(dj))
}

func TestStatusUnMarshler(t *testing.T) {
	dataJ := `
	{
		"id": 0,
		"created_at": 0,
		"updated_at": 0,
		"pulished_at": 0,
		"title": "",
		"author": "",
		"summary": "",
		"content": "",
		"status": "published"
	}
	`
	b := blog.NewBlog(blog.NewCreateBlogRequest())
	err := json.Unmarshal([]byte(dataJ), b)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(b.Status)
}
