package api

import (
	"net/http"
	"strconv"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/blog"
	"github.com/gin-gonic/gin"
)

// 把HTTP的用户请求 转换成内部的业务逻辑调用

// 如何测试你这个功能是否正常: Postman
func (h *handler) CreateBlog(c *gin.Context) {
	req := blog.NewCreateBlogRequest()
	// 如何获取 用户通过http body传递过来的请求数据
	// 这样就读取到 http body的数据 io.ReadAll(c.Request.Body)
	// 每个HTTP 框架 Objct -- Body

	if err := c.BindJSON(req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"msg": err.Error()})
		return
	}

	ins, err := h.svc.CreateBlog(c.Request.Context(), req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}

	c.JSON(http.StatusOK, ins)
}

// GET, 参数通过URL QueryString ?a=b&c=d
func (h *handler) QueryBlog(c *gin.Context) {

	// 参数解析逻辑
	req, err := blog.NewQueryBlogRequestFromGin(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"msg": err.Error()})
		return
	}

	set, err := h.svc.QueryBlog(c.Request.Context(), req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}
	c.JSON(http.StatusOK, set)
}

func (h *handler) DescribeBlog(c *gin.Context) {
	strId := c.Param("id")
	id, err := strconv.Atoi(strId)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"msg": err.Error()})
		return
	}
	req := blog.NewDescribeBlogRequest(id)
	ins, err := h.svc.DescribeBlog(c.Request.Context(), req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}

	c.JSON(http.StatusOK, ins)
}

// TOOD(作业)
func (h *handler) PutBlog(c *gin.Context) {
	strId := c.Param("id")
	id, err := strconv.Atoi(strId)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"msg": err.Error()})
		return
	}

	req := blog.NewUpdateBlogRequest(blog.PUT, id, blog.NewCreateBlogRequest())
	if err := c.BindJSON(req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"msg": err.Error()})
		return
	}

	ins, err := h.svc.UpdateBlog(c.Request.Context(), req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}

	c.JSON(http.StatusOK, ins)
}

// TOOD(作业)
func (h *handler) PatchBlog(c *gin.Context) {
	strId := c.Param("id")
	id, err := strconv.Atoi(strId)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"msg": err.Error()})
		return
	}

	req := blog.NewUpdateBlogRequest(blog.PATCH, id, blog.NewCreateBlogRequest())
	if err := c.BindJSON(req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"msg": err.Error()})
		return
	}

	ins, err := h.svc.UpdateBlog(c.Request.Context(), req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}

	c.JSON(http.StatusOK, ins)
}

// TOOD(作业)
func (h *handler) DeleteBlog(c *gin.Context) {
	strId := c.Param("id")
	id, err := strconv.Atoi(strId)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"msg": err.Error()})
		return
	}

	req := blog.NewDeleteBlogRequest(id)
	ins, err := h.svc.DeleteBlog(c.Request.Context(), req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}

	c.JSON(http.StatusOK, ins)
}
