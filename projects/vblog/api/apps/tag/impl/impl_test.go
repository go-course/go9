package impl_test

import (
	"context"
	"testing"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/tag"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/test/tools"
)

// 单元测试需要测试的对象是什么?
var (
	// 测试实现了Tag Service的对象
	impl tag.Service
	ctx  = context.Background()
)

func TestCreateTag(t *testing.T) {
	req := tag.NewCreateTagRequest("分类", "GO语言", 1)
	ins, err := impl.CreateTag(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryTag(t *testing.T) {
	req := tag.NewQueryTagRequest()
	// req.BlogIds = []int{0}
	req.Add(1)
	ins, err := impl.QueryTag(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

// 引用这个包 就会执行, 用于包的初始化
// 测试用例的包，需要初始化什么?
// 需要初始化被测试的对象
func init() {
	// 这个对象在ioc, ioc 需要提前初始化
	// 加载测试用例的配置对象
	tools.DevelopmentSet()
	// ioc里面获取的tag service的具体实现
	impl = apps.GetInternalApp(tag.AppName).(tag.Service)
}
