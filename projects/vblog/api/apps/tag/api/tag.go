package api

import (
	"net/http"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/tag"
	"github.com/gin-gonic/gin"
)

func (h *handler) CreateTag(c *gin.Context) {
	req := tag.NewDefaultCreateTagRequest()
	// 读取请求
	if err := c.BindJSON(req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"msg": err.Error()})
		return
	}

	ins, err := h.svc.CreateTag(c.Request.Context(), req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})
		return
	}

	// 响应
	c.JSON(http.StatusOK, ins)
}

// 作业
func (h *handler) RemoveTag(c *gin.Context) {

}
