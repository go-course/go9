package impl

import (
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/comment"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/conf"
	"google.golang.org/grpc"
	"gorm.io/gorm"
)

var _ comment.RPCServer = (*impl)(nil)

// 评论管理实例类
type impl struct {
	// 也是一个连接池，是gorm封装过后的连接池
	db *gorm.DB

	// 同时需要满足grpc 注册能力
	comment.UnimplementedRPCServer
}

// 对实例进行初始化，保护依赖的注入
func (i *impl) Init() error {
	// 需要赋值又给db实例过来, db实例是全局共享的
	// debug 会把执行的sql 打印处理
	i.db = conf.C().MySQL.ORM().Debug()
	return nil
}

// 其他地方还需要依赖这个名字
func (i *impl) Name() string {
	return comment.AppName
}

func (i *impl) RegistryHandler(server *grpc.Server) {
	comment.RegisterRPCServer(server, i)
}

func init() {
	obj := &impl{}

	// 指标把对象托管到Ioc的Grpc map
	apps.RegistryGrpc(obj)

	// 内部的模块
	apps.Registry(obj)
}
