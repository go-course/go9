package comment

import (
	"encoding/json"
	"time"

	"github.com/rs/xid"
)

func NewCreateCommentRequest() *CreateCommentRequest {
	return &CreateCommentRequest{}
}

func NewComment(req *CreateCommentRequest) *Comment {
	return &Comment{
		Meta: NewMeta(),
		Spec: req,
	}
}

func NewMeta() *Meta {
	return &Meta{
		Id:       xid.New().String(),
		CreateAt: time.Now().Unix(),
	}
}

func (c *Comment) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		*Meta
		*CreateCommentRequest
	}{c.Meta, c.Spec})
}

func (req *CreateCommentRequest) Validate() error {
	return nil
}
