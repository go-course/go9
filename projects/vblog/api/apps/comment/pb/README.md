# 评论模块 proto定义

之前样例:
```sh
protoc -I=. --go_out=. --go_opt=module="gitee.com/go-course/go9/tree/master/micro/protobuf/pb" --go-grpc_out=. --go-grpc_opt=module="gitee.com/go-course/go9/tree/master/micro/protobuf/pb" *.proto
```


编译: 根目录: 项目目录
```sh
# -I 当前项目目录
# --go_out 生成的文件也输出到当前项目目录下
protoc -I=. --go_out=. --go_opt=module="gitee.com/go-course/go9/tree/master/projects/vblog/api" --go-grpc_out=. --go-grpc_opt=module="gitee.com/go-course/go9/tree/master/projects/vblog/api" apps/*/pb/*.proto
```
