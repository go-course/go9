package api

import (
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps"
	"gitee.com/go-course/go9/tree/master/projects/vblog/api/apps/comment"
	"github.com/gin-gonic/gin"
)

// 定义一个handler对象来 实现 HTTP接口
// 需要依赖业务逻辑, 业务实例类
type handler struct {
	svc comment.Service
}

func (h *handler) Init() error {
	// ioc 依赖, 需要注入 user servcve的实例类
	// 通过ioc获取依赖
	h.svc = apps.GetInternalApp(comment.AppName).(comment.Service)
	return nil
}

func (h *handler) Name() string {
	return comment.AppName
}

// 这个模块的API的前缀: /vblog/api/v1/comments
// blog handler 需要暴露的接口 注册给 gin root router
func (h *handler) RegistryHandler(r gin.IRouter) {
	r.POST("/", h.CreateComment)
}

func init() {
	apps.RegistryHttp(&handler{})
}
