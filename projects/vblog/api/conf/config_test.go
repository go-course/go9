package conf_test

import (
	"testing"

	"gitee.com/go-course/go9/tree/master/projects/vblog/api/conf"
)

func TestLoadConfigFromToml(t *testing.T) {
	err := conf.LoadConfigFromToml("./test/test.toml")
	if err != nil {
		t.Fatal(err)
	}
	// 你们t.Log 看不到打印结构, 点击run test
	// C:\Program Files\Go\bin\go.exe test -timeout 30s -run ^TestLoadConfigFromToml$ gitee.com/go-course/go9/tree/master/projects/vblog/api/conf -count=1 -v
	// -count=1 -v 你们需要配置vscode 添加前面2个参数
	// -count=1, 每次都正在执行, go test 有缓存
	// -v 表示打印debug信息, t.Log 就类似于 测试用例的debug
	// 怎么配置vscode添加着2个参数？追加如下配置
	// "go.testFlags": [
	//     "-count=1",
	//     "-v"
	// ]
	t.Log(conf.C().Auth)
}

// 这个测试用例 如何配置环境变量
// 点run test的时候，环境变量如何注入?
// 配置vscode run test 时候需要注入的环境变量
// Go: Test Env File
// Absolute path to a file containing environment variables definitions.
// File contents should be of the form key=value.
func TestLoadConfigFromEnv(t *testing.T) {
	err := conf.LoadConfigFromEnv()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(conf.C().Auth)
}
