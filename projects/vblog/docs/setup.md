# 环境准备

+ Go: 1.18
+ 开发工具: Vscode
+ Postman: 接口调试工具
+ MySQL: docker 搭建
+ MySQL UI: navicat12

## 搭建MySQL

```sh
$ docker run -p 3306:3306 -itd -e MARIADB_USER=vblog -e MARIADB_PASSWORD=123456 -e MARIADB_ROOT_PASSWORD=123456 --name mysql   mariadb:latest
```

详细文档请参考: [mariadb docker hub](https://hub.docker.com/_/mariadb)


## 初始化表结构

[Crate Table SQL](./sql/table.sql)