CREATE TABLE `blogs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '文章的Id',
  `title` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章标题',
  `author` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '作者',
  `created_at` int NOT NULL COMMENT '创建时间',
  `updated_at` int NOT NULL COMMENT '更新时间',
  `pulished_at` int NOT NULL COMMENT '发布时间',
  `summary` text COLLATE utf8mb4_general_ci NOT NULL COMMENT '概要',
  `content` text COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章内容',
  `status` tinyint NOT NULL COMMENT '文章状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_title` (`title`) COMMENT 'titile添加唯一键约束'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `tags` (
  `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'tag id, Hash(文章+标签key+标签Value)',
  `t_key` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签key',
  `t_value` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签的值',
  `blog_id` int NOT NULL COMMENT '文章Id',
  `created_at` int NOT NULL COMMENT '时间戳',
  PRIMARY KEY (`id`),
  KEY `idx_tag` (`t_key`,`t_value`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;