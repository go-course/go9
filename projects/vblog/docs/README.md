# 项目文档

+ [需求设计文档](./design.md)
+ [环境搭建文档](./setup.md)

## 项目目录风格

+ 分层式布局: MVC, 不易剥离, 不利于阅读维护dao.(Blog,Tag,User)
   + routers: 整个项目的路由 /vblog/api/v1/blogs --> Http Handler
   + (C)handlers/controllers: 项目所有的业务逻辑 blogs,tags,login handlers
   + (M)models: 项目所有的struct定义
   + daos: 对象数据转换, 所有和数据库交互的逻辑 都方法到这里
+ 功能(业务)分区布局: 偏向于Java风格
   + 业务A
     + rpc: 内部RPC接口
        grpc.go：grpc接口
     + api: 业务模块对外暴露的协议
        http.go: 通过HTTP协议对外提供服务
        业务.go: 具体的接口实现
     + impl: 接口的实现类, 业务逻辑实现层
        + 业务.go: 业务的具体实现
        + impl.go: 定义实现类
        + model.go: 针对数据存储时的结果
        + dao.go: 与数据库交互的逻辑
     + 定义接口: interface.go
     + 数据模型: model.go
   + 业务B
     + ...
   + 业务C
     + ...