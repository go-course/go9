import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";

import "./assets/main.css";

const app = createApp(App);

// 安装ArcoDesign UI组件
import ArcoVue from "@arco-design/web-vue";
// 引入ArcoDesign UI组件的样式
import "@arco-design/web-vue/dist/arco.css";
app.use(ArcoVue);
// 额外引入图标库
import ArcoVueIcon from "@arco-design/web-vue/es/icon";
app.use(ArcoVueIcon);

app.use(createPinia());
app.use(router);

app.mount("#app");
