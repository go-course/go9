import client from "./client";
import Cookies from "js-cookie";

// 封装一个User Login API
// data: {name: "xxx", pass: "xxx"}
// LOGIN 函数是一个API封装, 为了区分和js本地函数, 把他大小了
export function LOGIN(data) {
  // 使用封装好的http client进行网络请求
  return client({
    url: "/vblog/api/v1/user/login",
    method: "post",
    // basic auth
    auth: { username: data.name, password: data.pass },
  });
}

// 退出登录
export function LOGOUT() {
  // username 从cookie中获取
  const username = Cookies.get("username");
  Cookies.remove("username");

  // 使用封装好的http client进行网络请求
  return client({
    url: "/vblog/api/v1/user/logout",
    method: "post",
    // data 指定body参数
    data: { username },
  });
}

// AxiosRequestConfig 对象
// export interface AxiosRequestConfig<D = any> {
//     url?: string;
//     method?: Method | string;
//     baseURL?: string;
//     transformRequest?: AxiosRequestTransformer | AxiosRequestTransformer[];
//     transformResponse?: AxiosResponseTransformer | AxiosResponseTransformer[];
//     headers?: RawAxiosRequestHeaders | AxiosHeaders;
//     params?: any;
//     paramsSerializer?: ParamsSerializerOptions;
//     data?: D;
//     timeout?: Milliseconds;
//     timeoutErrorMessage?: string;
//     withCredentials?: boolean;
//     adapter?: AxiosAdapterConfig | AxiosAdapterConfig[];
//     auth?: AxiosBasicCredentials;
//     responseType?: ResponseType;
//     responseEncoding?: responseEncoding | string;
//     xsrfCookieName?: string;
//     xsrfHeaderName?: string;
//     onUploadProgress?: (progressEvent: AxiosProgressEvent) => void;
//     onDownloadProgress?: (progressEvent: AxiosProgressEvent) => void;
//     maxContentLength?: number;
//     validateStatus?: ((status: number) => boolean) | null;
//     maxBodyLength?: number;
//     maxRedirects?: number;
//     maxRate?: number | [MaxUploadRate, MaxDownloadRate];
//     beforeRedirect?: (options: Record<string, any>, responseDetails: {headers: Record<string, string>}) => void;
//     socketPath?: string | null;
//     httpAgent?: any;
//     httpsAgent?: any;
//     proxy?: AxiosProxyConfig | false;
//     cancelToken?: CancelToken;
//     decompress?: boolean;
//     transitional?: TransitionalOptions;
//     signal?: GenericAbortSignal;
//     insecureHTTPParser?: boolean;
//     env?: {
//       FormData?: new (...args: any[]) => object;
//     };
//     formSerializer?: FormSerializerOptions;
//   }
