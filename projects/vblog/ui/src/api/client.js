import axios from "axios";

// 初始化一个http客户端实例
// create an axios instance
const client = axios.create({
  // 由于前端配置代理, 所以不用使用baseURL
  // url = base url + request url
  // 如果使用代理模式: vite.config.js添加配置:
  // server: {
  //   proxy: {
  //     "/vblog/api": {
  //       target: "http://localhost:8050",
  //     },
  //   },
  // },
  // 使用代理模式是, 需要把请求指向到vite服务器
  // 没有配baseURL, 默认会把请求转交给Vite
  baseURL: "",
  // 执行Set Cookie逻辑, 后端CROS Access-Control-Allow-Origin 不能为*
  withCredentials: true,
  // HTTP请求超时时间
  timeout: 5000,
});

// client对象是支持类似于中间件机制: 拦截器
// 请求的拦截器
client.interceptors.request.use(
  // 请求参数, 请求拦截
  (request) => {
    return request;
  }
);

// 响应拦截器
client.interceptors.response.use(
  // response callback;
  // 当前请求成功(http status: 200)
  (response) => {
    console.log(response);
    // 只返回respose data部分
    return response.data;
  },
  // failed callback;
  // 当前请求失败(http status: 非200)
  (error) => {
    // 把这个error给传递下去
    console.log(error);

    // 处理:AxiosError, 解析Error, 处理系统Error
    let errMsg = `${error.code}: ${error.message}`;

    // 处理业务Error, api返回的Error
    if (error.response.data) {
      errMsg = error.response.data.message;
    }
    return Promise.reject(new Error(errMsg));
  }
);

// 导出该client 对象
export default client;
