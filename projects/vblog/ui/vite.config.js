import { fileURLToPath, URL } from "node:url";

// 产物分块策略 splitVendorChunkPlugin:
// https://cn.vitejs.dev/guide/build.html#chunking-strategy
import { defineConfig, splitVendorChunkPlugin } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), splitVendorChunkPlugin()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  build: {
    outDir: "../api/ui/dist",
  },
  server: {
    proxy: {
      "/vblog/api/": "http://localhost:8050/",
    },
  },
});
