package impl

import (
	"context"

	"gitee.com/go-course/go9/projects/devcloud/maudit/apps/audit"
	"gitee.com/go-course/go9/projects/devcloud/maudit/common/logger"
)

// 日志保存
func (i *impl) SaveAuditLog(ctx context.Context, in *audit.AuditLog) (
	*audit.AuditLog, error) {
	logger.L().Debug().Msgf("save log: %s", in)
	return nil, nil
}

// 日志查询
func (i *impl) QueryAuditLog(ctx context.Context, in *audit.QueryAuditLogRequest) (
	*audit.AuditLogSet, error) {
	return nil, nil
}
