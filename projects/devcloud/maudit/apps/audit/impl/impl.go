package impl

import (
	"gitee.com/go-course/go9/projects/devcloud/maudit/apps/audit"
	"gitee.com/go-course/go9/projects/devcloud/maudit/conf"
	"github.com/infraboard/mcube/app"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	svc = &impl{}
)

// user service 的实例类？
// 之前如果进行实例类托管的, ioc 需要抽象到一个公共代码库管理
// mcube app
type impl struct {
	// 依赖数据库, 在配置对象里面
	col *mongo.Collection
}

// 实例类初始化
func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection(audit.AppName)

	return nil
}

func (i *impl) Name() string {
	return audit.AppName
}

func init() {
	// 注册内部服务托管类
	app.RegistryInternalApp(svc)
}
