package controller_test

import (
	"context"
	"testing"
	"time"

	"gitee.com/go-course/go9/projects/devcloud/maudit/controller"
	"gitee.com/go-course/go9/projects/devcloud/maudit/test/tools"
)

var (
	ctx = context.Background()
)

func TestAuduitLogSaveConroller(t *testing.T) {
	c := controller.NewAuduitLogSaveConroller(
		[]string{"localhost:9092"},
		"consumer-group-test",
		"topic-A",
	)

	go c.Run(ctx)

	time.Sleep(300 * time.Second)
	err := c.Stop()
	if err != nil {
		t.Fatal(err)
	}
}

func init() {
	tools.DevelopmentSetup()
}
