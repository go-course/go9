// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.21.6
// source: apps/deploy/pb/rpc.proto

package deploy

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	RPC_QueryDeployment_FullMethodName        = "/infraboard.mpaas.deploy.RPC/QueryDeployment"
	RPC_DescribeDeployment_FullMethodName     = "/infraboard.mpaas.deploy.RPC/DescribeDeployment"
	RPC_UpdateDeploymentStatus_FullMethodName = "/infraboard.mpaas.deploy.RPC/UpdateDeploymentStatus"
)

// RPCClient is the client API for RPC service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type RPCClient interface {
	QueryDeployment(ctx context.Context, in *QueryDeploymentRequest, opts ...grpc.CallOption) (*DeploymentSet, error)
	DescribeDeployment(ctx context.Context, in *DescribeDeploymentRequest, opts ...grpc.CallOption) (*Deployment, error)
	UpdateDeploymentStatus(ctx context.Context, in *UpdateDeploymentStatusRequest, opts ...grpc.CallOption) (*Deployment, error)
}

type rPCClient struct {
	cc grpc.ClientConnInterface
}

func NewRPCClient(cc grpc.ClientConnInterface) RPCClient {
	return &rPCClient{cc}
}

func (c *rPCClient) QueryDeployment(ctx context.Context, in *QueryDeploymentRequest, opts ...grpc.CallOption) (*DeploymentSet, error) {
	out := new(DeploymentSet)
	err := c.cc.Invoke(ctx, RPC_QueryDeployment_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rPCClient) DescribeDeployment(ctx context.Context, in *DescribeDeploymentRequest, opts ...grpc.CallOption) (*Deployment, error) {
	out := new(Deployment)
	err := c.cc.Invoke(ctx, RPC_DescribeDeployment_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rPCClient) UpdateDeploymentStatus(ctx context.Context, in *UpdateDeploymentStatusRequest, opts ...grpc.CallOption) (*Deployment, error) {
	out := new(Deployment)
	err := c.cc.Invoke(ctx, RPC_UpdateDeploymentStatus_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RPCServer is the server API for RPC service.
// All implementations must embed UnimplementedRPCServer
// for forward compatibility
type RPCServer interface {
	QueryDeployment(context.Context, *QueryDeploymentRequest) (*DeploymentSet, error)
	DescribeDeployment(context.Context, *DescribeDeploymentRequest) (*Deployment, error)
	UpdateDeploymentStatus(context.Context, *UpdateDeploymentStatusRequest) (*Deployment, error)
	mustEmbedUnimplementedRPCServer()
}

// UnimplementedRPCServer must be embedded to have forward compatible implementations.
type UnimplementedRPCServer struct {
}

func (UnimplementedRPCServer) QueryDeployment(context.Context, *QueryDeploymentRequest) (*DeploymentSet, error) {
	return nil, status.Errorf(codes.Unimplemented, "method QueryDeployment not implemented")
}
func (UnimplementedRPCServer) DescribeDeployment(context.Context, *DescribeDeploymentRequest) (*Deployment, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DescribeDeployment not implemented")
}
func (UnimplementedRPCServer) UpdateDeploymentStatus(context.Context, *UpdateDeploymentStatusRequest) (*Deployment, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateDeploymentStatus not implemented")
}
func (UnimplementedRPCServer) mustEmbedUnimplementedRPCServer() {}

// UnsafeRPCServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to RPCServer will
// result in compilation errors.
type UnsafeRPCServer interface {
	mustEmbedUnimplementedRPCServer()
}

func RegisterRPCServer(s grpc.ServiceRegistrar, srv RPCServer) {
	s.RegisterService(&RPC_ServiceDesc, srv)
}

func _RPC_QueryDeployment_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(QueryDeploymentRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RPCServer).QueryDeployment(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RPC_QueryDeployment_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RPCServer).QueryDeployment(ctx, req.(*QueryDeploymentRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RPC_DescribeDeployment_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DescribeDeploymentRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RPCServer).DescribeDeployment(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RPC_DescribeDeployment_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RPCServer).DescribeDeployment(ctx, req.(*DescribeDeploymentRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RPC_UpdateDeploymentStatus_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateDeploymentStatusRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RPCServer).UpdateDeploymentStatus(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RPC_UpdateDeploymentStatus_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RPCServer).UpdateDeploymentStatus(ctx, req.(*UpdateDeploymentStatusRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// RPC_ServiceDesc is the grpc.ServiceDesc for RPC service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var RPC_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "infraboard.mpaas.deploy.RPC",
	HandlerType: (*RPCServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "QueryDeployment",
			Handler:    _RPC_QueryDeployment_Handler,
		},
		{
			MethodName: "DescribeDeployment",
			Handler:    _RPC_DescribeDeployment_Handler,
		},
		{
			MethodName: "UpdateDeploymentStatus",
			Handler:    _RPC_UpdateDeploymentStatus_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "apps/deploy/pb/rpc.proto",
}
