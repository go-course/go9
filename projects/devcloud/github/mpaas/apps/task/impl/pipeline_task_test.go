package impl_test

import (
	"testing"

	"github.com/infraboard/mpaas/apps/build"
	"github.com/infraboard/mpaas/apps/job"
	"github.com/infraboard/mpaas/apps/pipeline"
	"github.com/infraboard/mpaas/apps/task"
	"github.com/infraboard/mpaas/test/conf"
	"github.com/infraboard/mpaas/test/tools"
)

func TestQueryPipelineTask(t *testing.T) {
	req := task.NewQueryPipelineTaskRequest()
	set, err := impl.QueryPipelineTask(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(set))
}

func TestRunTestPipeline(t *testing.T) {
	req := pipeline.NewRunPipelineRequest(conf.C.CICD_PIPELINE_ID)
	req.RunBy = "test"
	ins, err := impl.RunPipeline(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToYaml(ins))
}

func TestRunCICDPipeline(t *testing.T) {
	req := pipeline.NewRunPipelineRequest(conf.C.CICD_PIPELINE_ID)
	req.RunBy = "test"
	req.RunParams = job.NewRunParamWithKVPaire(
		// docker build 参数
		"GIT_SSH_URL", "git@gitlab.com:infraboard/mcenter.git",
		"GIT_BRANCH", "master",
		"GIT_COMMIT_ID", "7133ea72c41404b1464e9aadf4739b772febb9af",
		build.SYSTEM_VARIABLE_IMAGE_REPOSITORY, "registry.cn-hangzhou.aliyuncs.com/infraboard/mcenter",
		build.SYSTEM_VARIABLE_APP_VERSION, "v0.0.2",
		// docker deploy参数
		job.SYSTEM_VARIABLE_DEPLOY_ID, conf.C.MCENTER_DEPLOY_ID,
		// 和build参数一致, 不需要传递
		// build.SYSTEM_VARIABLE_IMAGE_REPOSITORY, "registry.cn-hangzhou.aliyuncs.com/infraboard/mcenter",
		// build.SYSTEM_VARIABLE_APP_VERSION, "v0.0.2",
	)

	ins, err := impl.RunPipeline(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToYaml(ins))
}

func TestDescribePipelineTask(t *testing.T) {
	req := task.NewDescribePipelineTaskRequest(conf.C.PIPELINE_TASK_ID)
	ins, err := impl.DescribePipelineTask(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToJson(ins))
}

func TestDeletePipelineTask(t *testing.T) {
	req := task.NewDeletePipelineTaskRequest(conf.C.PIPELINE_TASK_ID)
	ins, err := impl.DeletePipelineTask(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tools.MustToYaml(ins))
}
