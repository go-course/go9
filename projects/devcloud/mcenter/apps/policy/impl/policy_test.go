package impl_test

import (
	"testing"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/policy"
)

func TestCreatePolicy(t *testing.T) {
	req := policy.NewCreatePolicyRequest()
	req.Namespace = "default"
	req.RoleId = "cg5uklh3n7pjo26e8gtg"
	req.UserId = "cfotd7p3n7pjgb2t4lig"
	ins, err := impl.CreatePolicy(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryPolicy(t *testing.T) {
	req := policy.NewQueryPolicyRequest()
	req.WithRole = true
	ins, err := impl.QueryPolicy(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}
