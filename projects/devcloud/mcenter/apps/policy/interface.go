package policy

import (
	"gitee.com/go-course/go9/projects/devcloud/mcenter/common/meta"
	"github.com/infraboard/mcube/http/request"
)

const (
	AppName = "policy"
)

type Service interface {
	RPCServer
}

func NewCreatePolicyRequest() *CreatePolicyRequest {
	return &CreatePolicyRequest{}
}

func New(req *CreatePolicyRequest) *Policy {
	return &Policy{
		Meta: meta.NewMeta(),
		Spec: req,
	}
}

func NewQueryPolicyRequest() *QueryPolicyRequest {
	return &QueryPolicyRequest{
		Page: request.NewDefaultPageRequest(),
	}
}
