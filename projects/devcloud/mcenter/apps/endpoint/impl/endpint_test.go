package impl_test

import (
	"testing"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/endpoint"
)

func TestRegistryEndpoint(t *testing.T) {
	req := &endpoint.RegistryRequest{
		Items: []*endpoint.CreateEndpointRequest{},
	}
	req.Items = append(req.Items, &endpoint.CreateEndpointRequest{
		ServiceId: "service02",
		Method:    "POST",
		Path:      "xxxx",
		Operation: "xxx",
	})
	set, err := impl.RegistryEndpoint(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func TestQueryEnpoint(t *testing.T) {
	req := endpoint.NewQueryEnpointRequest()
	set, err := impl.QueryEnpoint(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
