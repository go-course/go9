package endpoint

import (
	"fmt"
	"hash/fnv"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/common/meta"
)

func NewEndpiontSet() *EndpointSet {
	return &EndpointSet{
		Items: []*Endpoint{},
	}
}

func (s *EndpointSet) Add(item *Endpoint) {
	s.Items = append(s.Items, item)
}

func NewRegistryRequest() *RegistryRequest {
	return &RegistryRequest{
		Items: []*CreateEndpointRequest{},
	}
}

func (r *CreateEndpointRequest) UnitKey() string {
	return fmt.Sprintf("%s.%s.%s", r.ServiceId, r.Method, r.Path)
}

func (s *RegistryRequest) Add(item *CreateEndpointRequest) {
	s.Items = append(s.Items, item)
}

func New(spec *CreateEndpointRequest) *Endpoint {
	ep := &Endpoint{
		Meta: meta.NewMeta(),
		Spec: spec,
	}
	h := fnv.New32a()
	_, err := h.Write([]byte(ep.Spec.UnitKey()))
	if err != nil {
		panic(err)
	}
	ep.Meta.Id = fmt.Sprintf("%d", h.Sum32())
	return ep
}

func NewCreateEndpointRequest() *CreateEndpointRequest {
	return &CreateEndpointRequest{}
}

func NewDefaultEndpoint() *Endpoint {
	return &Endpoint{
		Meta: meta.NewMeta(),
		Spec: NewCreateEndpointRequest(),
	}
}
