package endpoint

import "github.com/infraboard/mcube/http/request"

const (
	AppName = "endpoints"
)

type Service interface {
	RPCServer
}

func NewQueryEnpointRequest() *QueryEnpointRequest {
	return &QueryEnpointRequest{
		Page: request.NewDefaultPageRequest(),
	}
}
