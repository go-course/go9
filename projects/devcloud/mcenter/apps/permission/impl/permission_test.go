package impl_test

import (
	"testing"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/permission"
)

// user_id:"cfotd7p3n7pjgb2t4lig"
// namespace:"default"
// service_id:"cfsrgnh3n7pi7u2is87g"
// http_method:"GET"
// http_path:"/mpaas/api/v1/clusters"
func TestCheckPermission(t *testing.T) {
	req := permission.NewCheckPermissionRequest()
	req.UserId = "cfotd7p3n7pjgb2t4lig"
	req.Namespace = "default"
	// mpaas service id
	req.ServiceId = "cfsrgnh3n7pi7u2is87g"
	req.HttpMethod = "GET"
	req.HttpPath = "/mpaas/api/v1/clusters"
	ins, err := impl.CheckPermission(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	if !ins.HasPermisson {
		t.Fatal(err)
	}
	t.Log(ins)
}
