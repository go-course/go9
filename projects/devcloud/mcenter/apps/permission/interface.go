package permission

const (
	AppName = "permissions"
)

type Service interface {
	RPCServer
}

func NewCheckPermissionRequest() *CheckPermissionRequest {
	return &CheckPermissionRequest{}
}

func NewCheckPermissionResponse() *CheckPermissionResponse {
	return &CheckPermissionResponse{}
}
