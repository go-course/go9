package impl

import (
	"context"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/token"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/token/provider"
	"github.com/infraboard/mcube/exception"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// 令牌颁发: Restful
func (i *impl) IssueToken(ctx context.Context, in *token.IssueTokenRequest) (
	*token.Token, error) {
	// 参数校验
	if err := in.Validate(); err != nil {
		return nil, err
	}

	// 令牌的颁发
	issuer := provider.Get(in.GrantType)
	ins, err := issuer.IssueToken(ctx, in)
	if err != nil {
		return nil, err
	}

	// 存储
	_, err = i.col.InsertOne(ctx, ins)
	if err != nil {
		return nil, err
	}

	return ins, nil
}

// 令牌的校验: Grpc
func (i *impl) ValidateToken(ctx context.Context, in *token.ValidateTokenRequest) (
	*token.Token, error) {
	// 查出Token
	tk := token.NewToken()
	err := i.col.FindOne(ctx, bson.M{"_id": in.AccessToken}).Decode(tk)
	if err != nil {
		// 生成一个自定义异常
		// mongo: no documents in result
		// mongo: no documents in result
		if err == mongo.ErrNoDocuments {
			return nil, exception.NewNotFound("%s not found", in.AccessToken)
		}
		return nil, err
	}

	// 判断Token状态
	err = tk.CheckAliable()
	if err != nil {
		return nil, err
	}

	return tk, nil
}
