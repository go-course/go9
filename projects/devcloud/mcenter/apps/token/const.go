package token

const (
	// 标准认证头
	TOKEN_HEADER_KEY = "Authorization"

	// 上下文中Token key 的名称
	ATTRIBUTE_TOKEN_KEY = "token.mcenter"
)
