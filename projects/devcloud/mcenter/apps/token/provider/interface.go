package provider

import (
	"context"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/token"
)

type Issuer interface {
	Config() error
	TokenIssuer
}

type TokenIssuer interface {
	IssueToken(context.Context, *token.IssueTokenRequest) (*token.Token, error)
}
