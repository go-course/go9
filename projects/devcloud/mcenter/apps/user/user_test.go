package user_test

import (
	"testing"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/user"
)

func TestNewCreateUserRequest(t *testing.T) {
	req := user.NewCreateUserRequest()
	req.Password = "123456"
	err := req.HashPassword()
	if err != nil {
		t.Log(err)
	}
	t.Log(req.Password)
}
