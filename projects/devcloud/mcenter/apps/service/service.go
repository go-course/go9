package service

import (
	"time"

	"gitee.com/go-course/go9/projects/devcloud/mcenter/common/meta"
	"github.com/rs/xid"
)

func New(in *CreateServiceRequest) *Service {
	return &Service{
		Meta: meta.NewMeta(),
		Spec: in,
		Credentail: &Credentail{
			ClientId:     xid.New().String(),
			ClientSecret: xid.New().String(),
			CreateAt:     time.Now().Unix(),
		},
	}
}

func NewDefaultUser() *Service {
	return New(&CreateServiceRequest{})
}
