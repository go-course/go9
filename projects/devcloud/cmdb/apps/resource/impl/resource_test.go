package impl_test

import (
	"testing"
	"time"

	"gitee.com/go-course/go9/projects/devcloud/cmdb/apps/resource"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/domain"
	"gitee.com/go-course/go9/projects/devcloud/mcenter/apps/namespace"
)

func TestResourcePut(t *testing.T) {
	res := resource.NewResource()

	meta := resource.NewMeta()
	res.Meta = meta
	meta.Id = "ins-02"
	meta.CreateAt = time.Now().Unix()
	meta.Domain = domain.DEFAULT_DOMAIN
	meta.Namespace = namespace.DEFAULT_NAMESPACE
	meta.Env = "PROD"

	spec := resource.NewSpec()
	res.Spec = spec
	spec.Name = "测试3"
	spec.Cpu = 2
	spec.Memory = 4096

	status := resource.NewStatus()
	res.Status = status
	status.Phase = "Running"
	status.PrivateAddress = []string{"10.10.10.11"}

	res.AddTag(resource.NewKVTag(resource.TAG_PURPOSE_THIRDPARTY, "app", "app021"))
	ins, err := impl.Put(ctx, res)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestResourceSearch(t *testing.T) {
	req := resource.NewSearchRequest()
	// req.Tag["app"] = "app021"
	set, err := impl.Search(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
