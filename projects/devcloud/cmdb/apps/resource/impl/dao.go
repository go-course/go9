package impl

import "gitee.com/go-course/go9/projects/devcloud/cmdb/apps/resource"

// resource_meta
type ResourceMeta struct {
	*resource.Meta
	*resource.ContentHash
}
