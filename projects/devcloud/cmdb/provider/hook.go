package provider

import (
	"context"
	"fmt"

	"gitee.com/go-course/go9/projects/devcloud/cmdb/apps/resource"
)

// 仅仅是打印下对象
var DefaultResourceHandleHook = func(ctx context.Context, res *resource.Resource) {
	fmt.Println(res)
}

// Provider 完成资源转换后调用该Hook
type ResourceHandleHook func(ctx context.Context, res *resource.Resource)
