package main

import (
	"fmt"
	"os"
	"script_exporter/collectors/rocketmq/collector"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/expfmt"
)

func main() {
	// 怎么测试一个Collector
	// 是把collector 注册到一个注册表, 再执行采集
	// 从而获取采集结果

	// 新注册表
	registry := prometheus.NewRegistry()

	// 注册RocketMQ collector
	c := collector.NewCollector("samples/rocketmq.txt")
	registry.MustRegister(c)

	// 执行采集, 获取一个组metric
	mf, err := registry.Gather()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// 打印这些Metric 编码输出
	enc := expfmt.NewEncoder(os.Stdout, expfmt.FmtText)
	// 遍历每一个metric 把他编码成FmtText, 也就是Prometheus文本格式
	// 编码过后会把结果输出到buffer里面
	for i := range mf {
		enc.Encode(mf[i])
	}
}
