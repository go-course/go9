package collector

import (
	"encoding/json"
	"strconv"
)

// 如何把一行数据解析成一个结构体
func ParseLine(line string) *RocketMQMetric {
	// Rows
	words := []string{}
	// 字符内部, 当满足单词，就是出现了空格或者 一行结束
	chars := []rune{}

	for _, c := range line {
		if c == ' ' {
			// 组装成单词 转移到words里面
			if len(chars) > 0 {
				words = append(words, string(chars))
				chars = []rune{}
			}
		} else {
			chars = append(chars, c)
		}
	}

	// 一行结束
	if len(chars) > 0 {
		words = append(words, string(chars))
	}

	// words --> metric
	// Count  #Version                 #Type  #Model          #TPS     #Diff Total
	return &RocketMQMetric{
		Group:     words[0],
		Count:     words[1],
		Version:   words[2],
		Type:      words[3],
		Model:     words[4],
		TPS:       words[5],
		DiffTotal: words[6],
	}
}

// #Group                            #Count  #Version                 #Type  #Model          #TPS     #Diff Total
// rocket_to_kafka                   8       V3_2_6_SNAPSHOT          PUSH   CLUSTERING      0        0
type RocketMQMetric struct {
	Group     string
	Count     string
	Version   string
	Type      string
	Model     string
	TPS       string
	DiffTotal string
}

func (r *RocketMQMetric) String() string {
	dj, _ := json.Marshal(r)
	return string(dj)
}

func (m *RocketMQMetric) Float64Count() float64 {
	c, err := strconv.ParseFloat(m.Count, 64)
	if err != nil {
		panic(err)
	}
	return c
}
func (m *RocketMQMetric) Float64Tps() float64 {
	c, err := strconv.ParseFloat(m.TPS, 64)
	if err != nil {
		panic(err)
	}
	return c
}
func (m *RocketMQMetric) Float64DiffTotal() float64 {
	c, err := strconv.ParseFloat(m.DiffTotal, 64)
	if err != nil {
		panic(err)
	}
	return c
}
