# 用于存放 可以执行的模块

脚本输出必须符合Prom数据格式:
```
# HELP go_goroutines Number of goroutines that currently exist.
# TYPE go_goroutines gauge
go_goroutines 19
```