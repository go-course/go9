package script_test

import (
	"context"
	"script_exporter/excutor/script"
	"testing"
)

var (
	ctx     = context.Background()
	excutor = script.NewExcutor("modules")
)

func TestExec(t *testing.T) {
	req := script.NewExecRequest()
	req.ModuleName = "test.sh"
	err := excutor.Exec(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
}

func TestFindModule(t *testing.T) {
	// \script_exporter\excutor\script\modules\test.sh
	// 测试用例的相对路径, 当前的单元测试目录
	modulePath, err := excutor.FindModule("test.sh")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(modulePath)
}
