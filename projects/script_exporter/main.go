package main

import (
	"net/http"
	"script_exporter/excutor/script"
)

var (
	excutor = script.NewExcutor("modules")
)

// 架设一个HTTP API
func ScriptHandler(w http.ResponseWriter, r *http.Request) {
	req := script.NewExecRequest()
	qs := r.URL.Query()
	req.ModuleName = qs.Get("module_name")
	req.ModuleParams = qs.Get("module_params")
	req.Output = w
	err := excutor.Exec(r.Context(), req)
	if err != nil {
		w.Write([]byte(err.Error()))
	}
}

func main() {
	http.HandleFunc("/metrics", ScriptHandler)
	http.ListenAndServe(":8050", nil)
}
