package chat

import "github.com/infraboard/mcube/client/rest"

func NewChatRobot(ApiKey string) *ChatRobot {
	c := rest.NewRESTClient()
	c.SetBaseURL("https://api.openai.com/v1")
	c.SetBearerTokenAuth(ApiKey)
	return &ChatRobot{
		client: c,
	}
}

type ChatRobot struct {
	client *rest.RESTClient
}
