# web基础知识讲解

+ [Javascripts](https://gitee.com/infraboard/go-course/blob/master/day19/javascript.md)
+ HTML/CSS
+ Web基础(浏览器)
+ 前端框架: MVVM: vue3
+ vue全家桶: vue-router,pinia
+ vblog 前端

注意:
  需要直接实践操作, 对js进行熟悉

## Web开发环境准备

[环境准备](https://gitee.com/infraboard/go-course/blob/master/extra/devcloud/setup.md)

如何兼容多个nodejs版本: https://github.com/nvm-sh/nvm

```
$ node -v
v18.12.1

$ npm -v
8.18.0

$ npm install -g npm@9.2.0


$ yarn -v
1.22.19

$ npm install -g yrm
$ yrm -V
1.0.6

$ yrm use taobao

   YARN Registry has been set to: https://registry.npm.taobao.org/



   NPM Registry has been set to: https://registry.npm.taobao.org/

$ npx -v
9.2.0
```