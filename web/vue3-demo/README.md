# vue3-demo

This template should help get you started developing with Vue 3 in Vite.

vue 调用  后端restful API Demo

功能:  axios(浏览器中的 http client) ---> vblog api

+ [vue3官方文档](https://cn.vuejs.org/guide/quick-start.html)

```
cd vue3-demo
// 安装项目依赖
npm install
//  执行代码格式化, go fmt
npm run lint
// 启动前端项目
npm run dev
```

使用到的UI组件库: https://arco.design/vue/docs/start

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
