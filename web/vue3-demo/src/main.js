import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";

import "./assets/main.css";

const app = createApp(App);

// vue3 安装 ArcoDesgin UI组件库
import ArcoVue from "@arco-design/web-vue";
import "@arco-design/web-vue/dist/arco.css";
app.use(ArcoVue);

app.use(createPinia());
app.use(router);
// focus --> v-focus
app.directive("focus", {
  mounted: (el) => el.focus(),
});

// 全局组件注册
import TheWelcome from "@/components/TheWelcome.vue";
app.component("TheWelcome", TheWelcome);

app.mount("#app");
