// import {MyApp} from './profile.mjs'

// import default: export var __buildin__ = {}
// import { __buildin__ } from './profile.mjs'
// import { __buildin__ as pkg } from './profile.mjs'
import pkg from './profile.mjs'

// var firstName = 'sdsdfdf'

// 你知道profile这个包 有哪些变量被export, 指定引用哪些导出的变量 
console.log(pkg.firstName, pkg.lastName, pkg.year)

var a = ['A', 'B', 'C'];

// function callback(item) {
//     console.log(item)
// }
// 这个逻辑是串行的
// for (var x of a) {
//     // console.log(x); // '0', '1', '2', 'name'
//     callback(x)
// }

// 逻辑是并发的
// ('A') = {}, ('B') = {}, ('C') = {}
// a.forEach((item) => {
//     console.log(item)
// })
// console.log(a)



// function callback() {
//     console.log('Done');
// }
// console.log('before setTimeout()');
// // sleep
// setTimeout(callback, 1000); // 1秒钟后调用callback函数
// console.log('after setTimeout()');


function testResultCallbackFunc(resolve, reject) {
    var timeOut = Math.random() * 2;
    console.log('set timeout to: ' + timeOut + ' seconds.');
    // 下面这个逻辑是异步， 这个异步操作代表网络IO, 
    // 不能让这个网络IO直接阻塞函数的执行
    // 设计一个回调: 
    //  当执行成功的时候 调用resolve 函数来处理
    //  当执行失败的时候, 调用reject 函数来处理              
    setTimeout(function () {
        if (timeOut < 1) {
            console.log('call resolve()...');
            resolve('200 OK');
        }
        else {
            console.log('call reject()...');
            reject('timeout in ' + timeOut + ' seconds.');
        }
    }, timeOut * 1000);
}


// testResultCallbackFunc(
//     // 成功回调: 就打印一下返回的数据
//     (resp) => {console.log(resp)},
//     // 失败回调: 打印下结果
//     (error) => {console.log(error)}
// )


// 基于回调的Promise编程规范, 定义了一个异步函数后，可以通过Promise 构造成一个Promise对象
// var p1 = new Promise(testResultCallbackFunc)

// p1.
// then((resp) => {console.log(resp)}).
// catch((error) => {console.log(error)})


// async + await
async function testWithAsync() {
    // 这是一个异步操作(Promise对象都是异步)
    // p1.then().catch()
    var p1 = new Promise(testResultCallbackFunc)

    try {
        var resp = await p1
        console.log(resp)
    } catch (err) {
        console.log(err)
    }
}

testWithAsync()