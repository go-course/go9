package main

import (
	"fmt"
	"log"
	"net/rpc"
)

func main() {
	// 首先是通过rpc.Dial拨号RPC服务, 建立连接
	// 封装了 tcp 的rpc
	client, err := rpc.Dial("tcp", "localhost:1234")
	if err != nil {
		log.Fatal("dialing:", err)
	}

	// 建立连接后，如何调用Server function
	var resp string
	err = client.Call("GreetService.Greet", "Tom", &resp)
	if err != nil {
		panic(err)
	}

	fmt.Println(resp)
}
