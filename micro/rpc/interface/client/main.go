package main

import (
	"fmt"
	"net/rpc"

	"gitee.com/go-course/go9/tree/master/micro/rpc/interface/service"
)

// 约束客户端
var _ service.HelloService = (*HelloServiceClient)(nil)

// address 服务端地址
func NewHelloServiceClient(address string) *HelloServiceClient {
	// 首先是通过rpc.Dial拨号RPC服务, 建立连接
	// 封装了 tcp 的rpc
	client, err := rpc.Dial("tcp", address)
	if err != nil {
		panic(err)
	}
	return &HelloServiceClient{
		rpc: client,
	}
}

// 定义一个类,客户端的一个SDK
type HelloServiceClient struct {
	// 封装底层的rpc client
	rpc *rpc.Client
}

// 封装底层的Rpc逻辑, 调用原生请求
func (c *HelloServiceClient) Greet(request string, response *string) error {
	//  建立连接后，如何调用Server function
	return c.rpc.Call("GreetService.Greet", request, response)
}

func main() {
	// 1. 初始化一个客户端
	client := NewHelloServiceClient("localhost:1234")

	var resp string
	// client.Call("GreetService.Greet", "Tom", &resp)
	err := client.Greet("Tom", &resp)
	if err != nil {
		panic(err)
	}

	fmt.Println(resp)

	// 首先是通过rpc.Dial拨号RPC服务, 建立连接
	// 封装了 tcp 的rpc
	// client, err := rpc.Dial("tcp", "localhost:1234")
	// if err != nil {
	// 	log.Fatal("dialing:", err)
	// }

	// // 建立连接后，如何调用Server function
	// var resp string
	// err = client.Call("GreetService.Greet", "Tom", &resp)
	// if err != nil {
	// 	panic(err)
	// }

	// fmt.Println(resp)
}
