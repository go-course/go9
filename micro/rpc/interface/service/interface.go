package service

// 客户端: client.Call("GreetService.Greet", "Tom", &resp)  --> Greet()
// Server端口: Greet(request string, response *string) error
type HelloService interface {
	Greet(request string, response *string) error
}
