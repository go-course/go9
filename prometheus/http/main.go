package main

import (
	"fmt"
	"net/http"
)

func HelloHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "lexporter_request_count{user=\"admin\"} 1000")
}

func main() {
	http.HandleFunc("/metrics", HelloHandler)
	http.ListenAndServe(":8050", nil)
}
