package collector_test

import (
	"os"
	"prometheus/collector"
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/expfmt"
)

func TestDemoCollector(t *testing.T) {
	// 创建一个自定义的注册表
	registry := prometheus.NewRegistry()

	// 注册自定义采集器
	registry.MustRegister(collector.NewDemoCollector())

	// 获取注册所有数据
	data, err := registry.Gather()
	if err != nil {
		panic(err)
	}

	// 编码输出
	enc := expfmt.NewEncoder(os.Stdout, expfmt.FmtText)
	enc.Encode(data[0])
	enc.Encode(data[1])
}
