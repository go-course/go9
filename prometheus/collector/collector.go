package collector

import (
	"github.com/prometheus/client_golang/prometheus"
)

func NewDemoCollector() *DemoCollector {
	return &DemoCollector{
		queueLengthDesc: prometheus.NewDesc(
			"magedu_mcube_demo_queue_length",
			"The number of items in the queue.",
			// 动态标签的key列表
			[]string{"instnace_id", "instnace_name"},
			// 静态标签
			prometheus.Labels{"module": "http-server"},
		),
		queueLength2Desc: prometheus.NewDesc(
			"magedu_mcube_demo_queue_length2",
			"The number of items in the queue.",
			// 动态标签的key列表
			[]string{"instnace_id", "instnace_name"},
			// 静态标签
			prometheus.Labels{"module": "http-server"},
		),
		// 动态标的value列表, 这里必须与声明的动态标签的key一一对应
		labelValues: []string{"mq_001", "kafka01"},
	}
}

// 实现Collector接口
type DemoCollector struct {
	queueLengthDesc  *prometheus.Desc
	queueLength2Desc *prometheus.Desc
	labelValues      []string
}

func (c *DemoCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.queueLengthDesc
	ch <- c.queueLength2Desc
}

func (c *DemoCollector) Collect(ch chan<- prometheus.Metric) {
	// queueLength指标的值
	ch <- prometheus.MustNewConstMetric(c.queueLengthDesc, prometheus.GaugeValue, 100, c.labelValues...)
	// queueLength2指标的值
	ch <- prometheus.MustNewConstMetric(c.queueLength2Desc, prometheus.GaugeValue, 200, c.labelValues...)
}
