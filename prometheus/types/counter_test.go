package types_test

import (
	"bytes"
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/expfmt"
)

func TestCounter(t *testing.T) {
	// 生成了一个Gauge类型的指标
	queueLength := prometheus.NewCounter(prometheus.CounterOpts{
		// Namespace, Subsystem, Name 会拼接成指标的名称: magedu_mcube_demo_queue_length
		// 其中Name是必填参数
		Namespace: "magedu",
		Subsystem: "mcube_demo",
		Name:      "queue_length",
		// 指标的描信息
		Help: "The number of items in the queue.",
		// 指标的标签
		ConstLabels: map[string]string{
			"module":   "http-server",
			"hostname": "host01",
		},
	})

	// 获取当前状态，通过Set设置该指标当前的值
	queueLength.Inc()   //1
	queueLength.Add(10) // 11

	// 新注册表
	registry := prometheus.NewRegistry()
	registry.MustRegister(queueLength)

	// 执行采集, 获取一个组metric
	mf, err := registry.Gather()
	if err != nil {
		t.Fatal(err)
	}

	// 打印这些Metric 编码输出
	b := bytes.NewBuffer([]byte{})
	enc := expfmt.NewEncoder(b, expfmt.FmtText)
	// 遍历每一个metric 把他编码成FmtText, 也就是Prometheus文本格式
	// 编码过后会把结果输出到buffer里面
	for i := range mf {
		enc.Encode(mf[i])
	}

	// buffer里面就是编码后的数据
	t.Log(b.String())
}
